FROM tangramor/nginx-php8-fpm
COPY nginx.conf /etc/nginx/nginx.conf
COPY default.conf /etc/nginx/conf.d/default.conf
RUN mkdir -p /var/www/hos
COPY /hos/* /var/www/hos/
RUN chown -R nginx:nginx /var/www/hos
